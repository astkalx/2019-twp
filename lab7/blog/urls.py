"""blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path

from article.views import archive, get_article, create_post, register_user, auth_user

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', archive, name='index'),
    re_path(r'^article/(?P<article_id>\d+)$', get_article, name='get_article'),
    re_path(r'^article/new$', create_post, name='create_post'),
    re_path(r'^register', register_user, name='register'),
    re_path(r'^auth', auth_user, name='auth'),
]
