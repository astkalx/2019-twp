from django.shortcuts import render
from django.http import Http404
from .models import Article


# Create your views here.
def archive(request):
    return render(request, 'archive.html', {"posts":Article.objects.all(), 'STATIC_URL':'./static/'})

def get_article(request, article_id):
    try:
        post = Article.objects.get(id=article_id)
        return render(request, 'article.html', {'post': post, 'STATIC_URL':'../static/'})
    except Article.DoesNotExist:
        raise Http404